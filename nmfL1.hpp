#include <iostream>
#include <fstream>
#include <limits>
#include <list>
#include <vector>

#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
//#include <helper_functions.h>
//#include <helper_cuda.h>

#include <math.h>
#include <time.h>
#include <cassert>
#include "algo.hpp"

template <class T>
class NMFL1: public MF<T> {
	private:
		using MF<T>::n;
		using MF<T>::m;
		using MF<T>::r;
		using MF<T>::D;
		using MF<T>::X;
		using MF<T>::Y;
		using MF<T>::C;
		using MF<T>::nOnes;
		using MF<T>::mOnes;
		using MF<T>::handle;

		/////////////////
		// Helper
		/////////////////
		T* A;
		T* B;
		T* R;
		T* rOnes;
		/////////////////

		const T gamma;
		const T lamX;
		const T lamY;
		const bool dens;

	public:
		using MF<T>::getRank;
		using MF<T>::valBinaryPenalizer;

		NMFL1(T*& _D, const size_t& _m, const size_t& _n, const size_t& _r,  const T& _p, const T& _eps, const T& _dens) : MF<T>(_D,_m,_n,_r,_p,_eps), gamma(T(1.000001)), lamX(0.0), lamY(0.0), dens(_dens){
			
			T *hrOnes;
			Matrix::alloc(hrOnes, r, 1, 1);
			checkCudaErrors(cudaMalloc((void **) &rOnes, r*sizeof(T)));
			cudaMemcpy(rOnes, hrOnes, r*sizeof(T), cudaMemcpyHostToDevice);
			delete[] hrOnes;

			checkCudaErrors(cudaMalloc((void **) &A, r*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &B, m*r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &R, r*r*sizeof(T)));
		}

		~NMFL1(){
			cudaFree(A);
			cudaFree(B);
			cudaFree(R);
			cudaFree(rOnes);
		}
		
		void handleRankInc(){
			cudaFree(A);
			cudaFree(B);
			cudaFree(R);
			cudaFree(rOnes);

			T *hrOnes;
			Matrix::alloc(hrOnes, r, 1, 1);
			checkCudaErrors(cudaMalloc((void **) &rOnes, r*sizeof(T)));
			cudaMemcpy(rOnes, hrOnes, r*sizeof(T), cudaMemcpyHostToDevice);
			delete[] hrOnes;

			checkCudaErrors(cudaMalloc((void **) &A, r*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &B, m*r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &R, r*r*sizeof(T)));
		}

		T objective() const {
			T Nsum = 0, Xsum=0, Ysum=0;

			MF<T>::compNoiseBinary();
			cublasSasum(handle, m*n, C, 1, &Nsum);
			cublasSasum(handle, m*r, Y, 1, &Ysum);
			cublasSasum(handle, r*n, X, 1, &Xsum);
			return Nsum + lamX*Xsum + lamY*Ysum;
		}

		T objective_relaxed() const {
			T rss = 0, Xsum=0, Ysum=0;

			MF<T>::compNoiseRelaxed();
			cublasSnrm2(handle, m*n, C, 1, &rss);
			cublasSasum(handle, m*r, Y, 1, &Ysum);
			cublasSasum(handle, r*n, X, 1, &Xsum);
			return 0.5*rss*rss + lamX*Xsum + lamY*Ysum + valBinaryPenalizer();
		}

		

		// X is r x n
		// Y is m x r
		void iterateX(){
			const T a = 1.0, b = 0.0;

			////////////////////////////////
			// UPDATE X
			////////////////////////////////
			//gradient
			MF<T>::compNoiseRelaxed();
			cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, r, n, m, &a, Y, m, C, m, &b, A, r);
			cublasSger(handle, r, n, &lamX, rOnes, 1, nOnes, 1, A, r);
			
			//stepsize
			T alpha = 0;
			cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, r, r, m, &a, Y, m, Y, m, &b, R, r);
			cublasSnrm2(handle, r*r, R, 1, &alpha); //alpha = ||Y^TY||
			alpha = -1/(alpha*gamma);

			//make step
			cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, r, n, &a, X, r, &alpha, A, r, X, r);

			thrust::device_ptr<T> XT(X);
			thrust::transform(XT, XT+(r*n), XT, proxDelta(-alpha));
		}

		// X is r x n
		// Y is m x r
		void iterateY(){
			const T a = 1.0, b = 0.0;

			////////////////////////////////
			// UPDATE Y
			////////////////////////////////
			//gradient
			MF<T>::compNoiseRelaxed();
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, m, r, n, &a, C, m, X, r, &b, B, m);
			cublasSger(handle, m, r, &lamY, mOnes, 1, rOnes, 1, B, m);
			
			//stepsize
			T beta = 0;
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, r, r, n, &a, X, r, X, r, &b, R, r);
			cublasSnrm2(handle, r*r, R, 1, &beta); //beta = ||X^TX||
			beta = -1/(beta*gamma);

			//do step
			cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, r, &a, Y, m, &beta, B, m, Y, m);

			thrust::device_ptr<T> YT(Y);
			thrust::transform(YT, YT+(r*m), YT, proxDelta(-beta));
		}

		void iterate(){
			if(n>m){
				iterateX();
				iterateY();
			} else {
				iterateY();
				iterateX();
			}
		}

		
		void optThreshold(const T& thres, T& _best, T& _x, T& _y, T& _r) {
			cudaMemcpy(A, X, r*n*sizeof(T), cudaMemcpyDeviceToDevice);
			cudaMemcpy(B, Y, m*r*sizeof(T), cudaMemcpyDeviceToDevice);
			
			bool first_round = true;
			_best = std::numeric_limits<T>::max();
			T rAct =-10;
			
			for(T x=1; x>=0.0; x-=thres){
				for(T y=1; y>=0.0; y-=thres){

					if(!first_round){
						cudaMemcpy(X, A, r*n*sizeof(T), cudaMemcpyDeviceToDevice);
						cudaMemcpy(Y, B, m*r*sizeof(T), cudaMemcpyDeviceToDevice);
					}

					this->round(x, y);
					getRank(rAct,0,dens);

					const T val = objective();
					if(val<=_best){
						_best = val;
						_x = x;
						_y = y;
						_r = rAct;
					}

					if(first_round){
						first_round = 0;
					}
				}
			}

			cudaMemcpy(X, A, r*n*sizeof(T), cudaMemcpyDeviceToDevice);
			cudaMemcpy(Y, B, m*r*sizeof(T), cudaMemcpyDeviceToDevice);
		}


		struct proxDelta{
			const T a;

			proxDelta(T _a) : a(_a){}

			__host__ __device__
			T operator()(const T& z) const {
				return (z<=0.5) ? max(T(0), z-a) : min(T(1), z+a);
			}
		};
};

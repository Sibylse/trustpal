#define checkCudaErrors(x) x

#include <cstring>
using std::memcpy;

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <list>
#include <math.h>
#include <limits>
#include <cassert>

#include "nmfL1.hpp"
#include "algo.hpp"

int main(int argc, char** argv){
	if(argc%2==0){
		std::cout << "usage: " << argv[0] << " --opt1 val1 --opt2 val2 .. --optn valn" << std::endl;
		return 500;
	}

	std::string filename = "";
	std::string Xfilename = "";
	std::string Yfilename = "";

	std::string superPimp = "superPimp";
	std::string nmfL2 = "nmfL2";
	std::string nmfL1 = "nmfL1";
	std::string nmf = "nmf";
	std::string algoname = nmfL1;

	bool dens = 1;
	size_t rInc = 4;
	size_t device = 0;
	size_t maxk = 0;
	size_t maxr = 200;
	size_t deltaStop =0;
	float thresinc = 0.1;
	float eps = 0.1;
	float p = 0.1;
	float lam = 0.001;
	float delta = 0.5;
	char sep = '\t';
	
	size_t d,m,n;
	float *D = 0;
	float *T = 0;

	size_t showObj = 0; //0: no output, 1:show function values, 2: display matrices

	for(size_t i=1; i<argc; i+=2){
		std::string opt(argv[i]);
		std::string val(argv[i+1]);

		if(!opt.compare("--rInc")) rInc = get<size_t>(val);
		else if(!opt.compare("--K")) maxk = get<size_t>(val);
		else if(!opt.compare("--D")) filename = val;
		else if(!opt.compare("--dens")) dens = get<bool>(val);
		else if(!opt.compare("--T")) Matrix::read(val, T, m, d, sep);
		else if(!opt.compare("--X")) Xfilename = val;
		else if(!opt.compare("--Y")) Yfilename = val;
		else if(!opt.compare("--A")) algoname = val;
		else if(!opt.compare("--maxR")) maxr = get<size_t>(val);
		else if(!opt.compare("--eps")) eps = get<float>(val);
		else if(!opt.compare("--delta")) delta = get<float>(val);
		else if(!opt.compare("--deltaStop")) deltaStop = get<float>(val);
		else if(!opt.compare("--p")) p = get<float>(val);
		else if(!opt.compare("--lam")) lam = get<float>(val);
		else if(!opt.compare("--dev")) device = get<size_t>(val);
		else if(!opt.compare("--t")) thresinc = get<float>(val);
		else if(!opt.compare("--sep")) sep = get<char>(val);
		else if(!opt.compare("--showobj")) showObj = get<size_t>(val);
		else{
			std::cout << "UNKNOWN OPTION: " << opt << std::endl;
			return 700;
		}
	}

	if(!filename.size() ){
		std::cout << "please provide D, X and Y matrices" << std::endl;
		return 300;
	}

	std::cout << "D\t" << filename << std::endl;
	std::cout << "X\t" << Xfilename << std::endl;
	std::cout << "Y\t" << Yfilename << std::endl;
	std::cout << "FDR DENSITY\t" << dens << std::endl;
	std::cout << "ITERS\t" << maxk << std::endl;
	std::cout << "RANKINC\t" << rInc << std::endl;
	std::cout << "THRESINC\t" << thresinc << std::endl;

	cudaDeviceProp dev;
	cudaGetDeviceProperties(&dev, device);
	cudaSetDevice(device);
	std::cout << "GPU\t" << dev.name << std::endl;

	const char pipe[4] = { '-', '\\', '|', '/' };
	
	double start = getTime();
	Matrix::read(filename, D, m, n, sep);
	std::cout << "READ DATA TOOK " << getTime()-start << "s" << std::endl;
	MF<float>* algo;

	
	//std::cout << "LAM\t" << lam << std::endl;
	algo = new NMFL1<float>(D, m, n, rInc, p, eps, dens);
	
	
	/*if(Xfilename.size() && Yfilename.size() ){
		start = getTime();
		Matrix::readGPU(Xfilename, algo->getX(), rInc, n);
		Matrix::readGPU(Yfilename, algo->getY(), m, rInc);
		std::cout << "READ X AND Y TOOK " << getTime()-start << "s" << std::endl;
	} else {
		std::cout << "INITIALIZE X AND Y RANDOMLY" << std::endl;
	}*/

	
	  /////////////////////
	 // ALGORITHM BODY  //
	/////////////////////
	float b, x, y, rEst;
	size_t r = rInc;
	int increased =1;
	start = getTime();
	float oldVal=std::numeric_limits<float>::max();
	while(increased){
		//Do Optimization 
		std::list<float> L;
		float SUM = 0;

		for(size_t k=0; k<maxk; ++k){
			algo->iterate();
			if(showObj==1) {
				float val = algo->objective_relaxed();

				if(k>0){
					L.push_back(abs(oldVal-val));
					if(L.size()>500){
						L.pop_front();
					}
					SUM=0;
					for(auto x : L)
						SUM += x;

					std::cout << std::fixed << std::setprecision(8);
					std::cout << "\r"<< k << "   " << val << "   " << oldVal-val << "   " << SUM/L.size() << "           " << std::flush;
				if(SUM/L.size()<delta)
					break;
				}
				//assert(oldVal-val>=-5e-1);
				oldVal = val;
			}
			else std::cout << "\rWORK\t" << pipe[k%4] << " (" << k << ")" << std::flush;
		}
		//Compare rank
		algo->optThreshold(thresinc, b, x, y, rEst);
		std::cout << "rEst:" << rEst << " r:"<< r << std::endl;
		//algo->round(x, y);
		if(showObj>1) algo->printMatrices();
		if(rEst+deltaStop<r || r>maxr){
			increased=0;
		}else{
			algo->incrRank(rInc);
			r+=rInc;
			oldVal = algo->objective_relaxed();
		}
	}
	std::cout << "\rWORK\tDONE!" << std::endl;
	algo->round(x, y);
	algo->getRank(rEst, 1, dens);
	if(showObj>1) {
		algo->printMatrices();
		std::cout << "(x,y)=("<<x<<","<<y<<")" << std::endl;
	}

	std::stringstream dfs;
	dfs <<  filename << algoname << ".K" << maxk;
	std::string fn = dfs.str();

	algo->writeOutput(fn);

	std::cout << "ESTIMATED RANK: " << rEst << std::endl;
	std::cout << "WRITE TO: " << fn << std::endl;
	std::cout << "RANK ESTIMATION TOOK " << getTime()-start << "s" << std::endl;

	delete algo;
	delete[] D;

	return r;
}

# TrustPal #

### How do I get set up? ###

Run the "build" script to build TrustPal. You might have to adjust the build command to the properties of your GPU or C++ libraries which are installed.
The runTest script invokes the MovieLens experiment according to your specifications. Simply run one of the following commands (the first uses the density bound and the second the coherence bound for model order selection):

```
./runTest  1 0 0.01
./runTest  0 0 0.01

```

The second argument 0 indicates the GPU device number, so you might want to adjust this one.

For TrustPal, you can specify the following parameters:

* --dens: use of density bound (--dens 1) or coherence bound (--dens 0)
* --rInc: the rank increment (default: 10)
* --K: the number of iterations (default: 1000)
* --D: the path to the data matrix, i.e., the (m x n) data matrix where m is the number of transactions
* --maxR: maximum rank to consider (default: 200)
* --dev: number of gpu device (default: 0)
* --delta: the minimum function decrease after which the optimization is aborted
* --t: the threshold increment at which matrices are rounded to binary values in the end (default: 0.05)
* --sep: the separator used in the table of D (default: '\t') 

The Julia scripts in the folder SCRIPTS can be used to generate synthetic data matrices and to plot values of the bounds under specified settings. You can run these scripts without installation with [JuliaBox](https://www.juliabox.org/ "juliabox").

### Who do I talk to? ###

* Sibylle Hess (sibylle.hess@tu-dortmund.de)
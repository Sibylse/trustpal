#ifndef __ALGO__
#define __ALGO__
#define checkCudaErrors(x) x

#include <sys/time.h>
#include <cmath>
#include <curand_kernel.h>

inline double getTime(){
	struct timeval tv;
	if (gettimeofday(&tv, NULL)==0)
		return tv.tv_sec+((double)(tv.tv_usec))/1e6;
	else
		return 0.0;
}

template <class T>
T get(std::string s){
	std::stringstream is(s);
	T t;
	is >> t;
	return t;
}

namespace Matrix{
	template <class T>
	void alloc(T*& A, const size_t& m, const size_t& n, const size_t& val = 0){
		A = new T[m*n];
		for(size_t i=0; i<m; ++i){
			for(size_t j=0; j<n; ++j)
				A[i*n+j] = val;
		}
	}

	template <class T>
	void print(T*& A, const size_t& m, const size_t& n, std::ostream& out = std::cout){
		out << std::fixed << std::setprecision(2);
		for(size_t j=0; j<m; ++j){
			for(size_t i=0; i<n; ++i)
				out << A[i*m+j] << ' ';
			out << std::endl;
		}
	}

	template <class T>
	void printGPU(T* A, const size_t& m, const size_t& n){
		T* hA;
		alloc(hA, m, n);
		cudaMemcpy(hA, A, m*n*sizeof(T), cudaMemcpyDeviceToHost);

		print(hA, m, n);

		delete hA;
	}

	template <class T>
	void writeGPU(std::string filename, T* A, const size_t& m, const size_t& n){
		T* hA;
		alloc(hA, m, n);
		cudaMemcpy(hA, A, m*n*sizeof(T), cudaMemcpyDeviceToHost);
		std::ofstream outfile(filename, std::ios::out | std::ios::binary);
		outfile.write((char*)hA, m*n*sizeof(T));
		outfile.close();
		delete hA;
	}

	template <class T>
	void read(std::string filename, T*& D, size_t& m, size_t& n, const char& sep){
		std::vector<std::vector<T>*>* indata = new std::vector<std::vector<T>*>();
		std::string line;

		std::ifstream datafile(filename);
		while(!datafile.eof()){
			getline(datafile, line);
			if(line.size()>1){
				std::vector<T>* rowlist = new std::vector<T>();

				std::stringstream ls(line);
				std::string vs;
				while(!ls.eof()){
					getline(ls, vs, sep);
					T val = get<T>(vs);
					rowlist->push_back(val);
				}

				indata->push_back(rowlist);
			}
			std::cout << '\r' << indata->size() << std::flush;
		}
		datafile.close();
		std::cout << std::endl;

		m = indata->size();
		n = indata->front()->size();

		Matrix::alloc(D, m, n);

		int i=0, j=0;
		for(std::vector<T>* l : *indata){
			j=0;
			for(auto x : *l){
				D[j*m+i] = x;
				++j;
			}
			++i;
			delete l;
		}
		delete indata;
	}

	template <class T>
	void read(std::string filename, T*& A, const size_t& m, const size_t& n){
		std::ifstream infile(filename, std::ios::in | std::ios::binary);
		infile.read((char*)A, m*n*sizeof(T));
		infile.close();
	}

	template <class T>
	void readGPU(std::string filename, T* A, const size_t& m, const size_t& n){
		T* hA;
		alloc(hA, m, n);
		std::ifstream infile(filename, std::ios::in | std::ios::binary);
		infile.read((char*)hA, m*n*sizeof(T));
		infile.close();
		cudaMemcpy(A, hA, m*n*sizeof(T), cudaMemcpyHostToDevice);
		delete hA;
	}

	template <class T>
	void fillI(T*& A, const size_t& m, const size_t& n){
		for(size_t i=0; i<m; ++i){
			for(size_t j=0; j<n; ++j)
				A[i*n+j] = (rand()/T(RAND_MAX));
		}
	}

	template <class T>
	void fillCombinations(T*& A, const size_t& m, const size_t& n){
		for(size_t i=0; i<n; ++i){ //for each class (column)
			int b = pow(2,i);
			int div = 2*b;		
			for(size_t j=0; j<m; ++j){
				A[i*m+j] = (j%div)<b;
			}
		}
	}

	template <class T>
	void fillRand(T*& A, const size_t& m, const size_t& n){
		srand(time(NULL));
		for(size_t i=0; i<n; ++i){
			for(size_t j=0; j<m; ++j){
					A[i*m+j] = (rand()/T(RAND_MAX));
					//A[i*m+j] = 0.5;
			}
		}
	}
	
	//Copies the mxn upper left tile from B into A
	template <class T>
	void copy(T*& B, const size_t& mA, const size_t& mB, const size_t& nB, T*& A){
		for(size_t j=0; j<mB; ++j){
			for(size_t i=0; i<nB; ++i)
				A[i*mA+j] = B[i*mB+j];
		}
	}
}

template <class T>
struct theta{
	const T a;

	theta(T _a = 0.5) : a(_a){}

	__host__ __device__
	T operator()(const T& z) const {
		return z>a;
	}
};

template <class T>
struct thetaRand{
	const T a;

	thetaRand(T _a = 0.5) : a(_a){}

	__device__
	T operator()(const T& z) const {

		//unsigned int seed = (unsigned int)(z*a+threadIdx.x);
		unsigned int seed = (unsigned int)(z*a);

		curandState s;

		// seed a random number generator
		curand_init(seed, 0, 0, &s);

		const T u = curand_uniform(&s);
		return u<=z;
	}
};

// the binary penalizing function (only correct for x in [0,1] - 
// after the prox application this is always the case)
template<class T> 
struct Lambda{

	__host__ __device__
	T operator()(const T& x) const {
		return -abs(0.5-x)+0.5;
	}
};

template <class T>
class MF{
	protected:
		T* X;
		T* Y;

		/////////////////
		// Helper
		/////////////////
		T* u;
		T* v;
		T* w;          // length r
		T* C;
		T* Cx;         // (nxn)
		T* Cy;	       // (mxm)
		/////////////////

		/////////////////
		// Constants
		/////////////////
		T* D;
		T* mOnes;
		T* nOnes;
		/////////////////

		const size_t m;
		const size_t n;
		size_t r;
		const T p;
		const T eps;

		cublasHandle_t handle;
		cublasStatus_t status;

	public:
		MF(T*& _D, const size_t& _m, const size_t& _n, const size_t& _r, const T& _p, const T& _eps) : m(_m), n(_n), r(_r), p(_p), eps(_eps){
			cublasCreate(&handle);
			cublasSetPointerMode(handle, CUBLAS_POINTER_MODE_HOST);

			T *hX, *hY, *hCx, *hCy, *hmOnes, *hnOnes;

			Matrix::alloc(hX, r, n);
			Matrix::alloc(hY, m, r);
			Matrix::alloc(hCx,n, n, 0);
			Matrix::alloc(hCy, m, m, 0);
			Matrix::alloc(hmOnes, m, 1, 1);
			Matrix::alloc(hnOnes, n, 1, 1);

			Matrix::fillRand(hX, r, n);
			Matrix::fillRand(hY, m, r);

			checkCudaErrors(cudaMalloc((void **) &Cx, n*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &Cy, m*m*sizeof(T)));
			cudaMemcpy(Cx, hCx, n*n*sizeof(T), cudaMemcpyHostToDevice);
			cudaMemcpy(Cy, hCy, m*m*sizeof(T), cudaMemcpyHostToDevice);

			checkCudaErrors(cudaMalloc((void **) &X, r*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &Y, m*r*sizeof(T)));

			checkCudaErrors(cudaMalloc((void **) &u, r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &v, r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &w, r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &C, m*n*sizeof(T)));

			checkCudaErrors(cudaMalloc((void **) &mOnes, m*sizeof(T)));
			cudaMemcpy(mOnes, hmOnes, m*sizeof(T), cudaMemcpyHostToDevice);

			checkCudaErrors(cudaMalloc((void **) &nOnes, n*sizeof(T)));
			cudaMemcpy(nOnes, hnOnes, n*sizeof(T), cudaMemcpyHostToDevice);

			cudaMemcpy(X, hX, r*n*sizeof(T), cudaMemcpyHostToDevice);
			cudaMemcpy(Y, hY, m*r*sizeof(T), cudaMemcpyHostToDevice);

			checkCudaErrors(cudaMalloc((void **) &D, m*n*sizeof(T)));
			cudaMemcpy(D, _D, m*n*sizeof(T), cudaMemcpyHostToDevice);

			delete[] hX;
			delete[] hY;
			delete[] hCx;
			delete[] hCy;
			delete[] hmOnes;
			delete[] hnOnes;
		}

		~MF(){
			cudaFree(X);
			cudaFree(Y);
			cudaFree(D);
			cudaFree(u);
			cudaFree(v);
			cudaFree(w);
			cudaFree(mOnes);
			cudaFree(nOnes);
			cudaFree(C);
			cudaFree(Cx);
			cudaFree(Cy);
			cudaDeviceReset();
		}


		virtual T objective() const = 0;

		virtual T objective_relaxed() const = 0;

		virtual void iterate() = 0;

		virtual void handleRankInc() = 0;

		virtual void optThreshold(const T& thres, T& _best, T& _x, T& _y, T& _r) = 0;
		
		//Computes C=YX-D
		virtual void compNoiseRelaxed() const {
			const T w = -1.0, a=1.0;
			cudaMemcpy(C, D, m*n*sizeof(T), cudaMemcpyDeviceToDevice);
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, r, &a, Y, m, X, r, &w, C, m);
		}

		//Computes C=theta(YX)-D
		virtual void compNoiseBinary() const {
			const T a = 1.0, b=0.0, w = -1.0;
			//C=theta(YX)
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, r, &a, Y, m, X, r, &b, C, m);
			thrust::device_ptr<T> ptrC(C);
			thrust::transform(ptrC, ptrC+(m*n), ptrC, theta<T>(0.5));
			//C=theta(YX)-D
			cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, &a, C, m, &w, D, m, C, m);
		}

		double lbinom(size_t n, size_t k) const {
			return lgamma((T)n+1) - lgamma((T)k+1) - lgamma((T)n-k+1);
		}

		double getlogPDens(const size_t l, const size_t k, const double dens) const {
			const double _dmp = dens-p;
			return lbinom(n,k) + lbinom(m,l) - 2*k*l * _dmp*_dmp;
		}

		double getlogPCohX(const double cohX) const {
			const double _max = std::max(0.0,cohX/n-p*p);
			return log(m)+log(m-1)-log(2) - (3.0/2.0)*n * _max*_max / (1.0-p*p) / (2.0*p*p+cohX/n);
		}

		double getlogPCohY(const double cohY) const {
			const double _max = std::max(0.0,cohY/m-p*p);
			return log(n)+log(n-1)-log(2) - (3.0/2.0)*m * _max*_max / (1.0-p*p) / (2.0*p*p+cohY/m);
		}
/*
		__global__ setOne(T* target, int idx){
			if(threadIdx.x==0 && blockIdx.x==0)
				target[idx] = (T)1.0;
		}
*/
		void allCoherences(bool out) {
			const T a = 1;
			const T b = 0;
			int cohXi=0, cohYi=0;
			//if(out)
			//	printMatrices();
			for (int s=0;s<r;s++){
				//C=diag(Y_s) x D x diag(X_s)
				cublasSdgmm(handle, CUBLAS_SIDE_LEFT, m, n, D, m, Y+s*m, 1, C, m);
				cublasSdgmm(handle, CUBLAS_SIDE_RIGHT, m, n, C, m, X+s, r, C, m);
				cublasSsyrk(handle, CUBLAS_FILL_MODE_UPPER, CUBLAS_OP_T, n, m, &a, C, m, &b, Cx, n);
				cublasSsyrk(handle, CUBLAS_FILL_MODE_UPPER, CUBLAS_OP_N, m, n, &a, C, m, &b, Cy, m);
				/*
				if(out){				
					std::cout << "\nCx: " << std::endl;
					Matrix::printGPU(Cx, n, n);
					std::cout << "\nCy: " << std::endl;
					Matrix::printGPU(Cy, m, m);
				}
				*/
				cublasSscal(handle, n, &b, Cx, n+1);
				cublasSscal(handle, m, &b, Cy, m+1);
				/*					
				if(out){
					std::cout << "\nCx wt diag: " << std::endl;
					Matrix::printGPU(Cx, n, n);
					std::cout << "\nCy wt diag: " << std::endl;
					Matrix::printGPU(Cy, m, m);
				}
				*/
				cublasIsamax(handle, n*n, Cx, 1, &cohXi);
				cublasIsamax(handle, m*m, Cy, 1, &cohYi);

				T cohX=0, cohY=0;

				cudaMemcpy(&cohX, Cx+cohXi-1, sizeof(T), cudaMemcpyDeviceToHost);
				cudaMemcpy(&cohY, Cy+cohYi-1, sizeof(T), cudaMemcpyDeviceToHost);
				if(out)
					std::cout << "\ncohX=" << cohX << ", cohY=" << cohY << std::endl;

				T logPCohX = getlogPCohX(cohX);
				T logPCohY = getlogPCohY(cohY);
				if(out)
					std::cout << "PX=" << exp(logPCohX)  << ", PY=" << exp(logPCohY) << std::endl;

				if (getlogPCohX(cohX) >= log(eps) && getlogPCohY(cohY) >= log(eps))  {
					//reject tile s
					cudaMemcpy(u+s, &b, sizeof(T), cudaMemcpyHostToDevice);
				}else{
					cudaMemcpy(u+s, &a, sizeof(T), cudaMemcpyHostToDevice);
				}
			}

		}

		void densityBound(bool out){
			const T a = 1;
			const T b = 0;
			cublasSgemv(handle, CUBLAS_OP_N, r, n, &a, X, r, nOnes, 1, &b, v, 1);
			cublasSgemv(handle, CUBLAS_OP_T, m, r, &a, Y, m, mOnes, 1, &b, w, 1);
			for (int s=0;s<r;s++){
	
				T dens=0.0;
				//C=diag(Y_s) x D x diag(X_s)
				cublasSdgmm(handle, CUBLAS_SIDE_LEFT, m, n, D, m, Y+s*m, 1, C, m);
				cublasSdgmm(handle, CUBLAS_SIDE_RIGHT, m, n, C, m, X+s, r, C, m);
				cublasSasum(handle, m*n, C, 1, &dens);

				T _k=0, _l=0;
				cudaMemcpy(&_k, v+s, sizeof(T), cudaMemcpyDeviceToHost);
				cudaMemcpy(&_l, w+s, sizeof(T), cudaMemcpyDeviceToHost);
				if(out)
					std::cout << "\nk=" << _k << ", l=" << _l << std::endl;
				T logP=0;
				if(_k*_l > 0)
					logP = getlogPDens(_l, _k, dens/_k/_l);
				if(out)
					std::cout << "P=" << exp(logP) << ", dens=" << dens/_k/_l << std::endl;

				if (logP >= log(eps))  {
					//reject tile s -> do nothing because coherence is first
					cudaMemcpy(u+s, &b, sizeof(T), cudaMemcpyHostToDevice);
				}else{
					cudaMemcpy(u+s, &a, sizeof(T), cudaMemcpyHostToDevice);
				}
			}

		}
		
		virtual void getRank(T& rEst, bool out, bool dens) {
			if(dens){
				densityBound(out);
			} else {			
				allCoherences(out);
			}
			cublasSdgmm(handle, CUBLAS_SIDE_LEFT, r, n, X, r, u, 1, X, r);
			cublasSdgmm(handle, CUBLAS_SIDE_RIGHT, m, r, Y, m, u, 1, Y, m);
			cublasSasum(handle, r, u, 1, &rEst);
		}

		virtual void round(const T& x, const T& y) {
			roundX(x);
			roundY(y);
		}

		virtual void writeOutput(std::string fn) {
			Matrix::writeGPU(fn+".X", X, r, n);
			Matrix::writeGPU(fn+".Y", Y, m, r);
		}

		void incrRank(const T& rInc){
			T *hXold, *hYold; 
			T *hX, *hY;

			Matrix::alloc(hX, r+rInc, n);
			Matrix::alloc(hY, m, r+rInc);
			Matrix::fillRand(hX, r+rInc, n);
			Matrix::fillRand(hY, m, r+rInc);
		
			//Insert mined clusters in new matrix on host device
			Matrix::alloc(hXold, r, n);
			Matrix::alloc(hYold, m, r);
			cudaMemcpy(hXold, X, n*r*sizeof(T), cudaMemcpyDeviceToHost);
			cudaMemcpy(hYold, Y, m*r*sizeof(T), cudaMemcpyDeviceToHost);
			Matrix::copy(hXold, r+rInc, r, n, hX);
			Matrix::copy(hYold, m, m, r, hY);
				
			r+=rInc;
			cudaFree(X);
			cudaFree(Y);
			cudaFree(u);
			cudaFree(v);
			cudaFree(w);

			checkCudaErrors(cudaMalloc((void **) &X, r*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &Y, m*r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &u, r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &v, r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &w, r*sizeof(T)));

			cudaMemcpy(X, hX, r*n*sizeof(T), cudaMemcpyHostToDevice);
			cudaMemcpy(Y, hY, m*r*sizeof(T), cudaMemcpyHostToDevice);
			
			delete[] hX;
			delete[] hY;
			delete[] hXold;
			delete[] hYold;

			handleRankInc();
		}

		T valBinaryPenalizer() const {
			thrust::plus<T> pl;
			thrust::device_ptr<T> ptrX(X);
			T binPenal = thrust::transform_reduce(ptrX, ptrX+r*n, Lambda<T>(), T(0),pl); 
			thrust::device_ptr<T> ptrY(Y);
			binPenal += thrust::transform_reduce(ptrY, ptrY+r*m, Lambda<T>(), T(0),pl);
			return binPenal;
		}	

		void roundX(const T& x) {
			thrust::device_ptr<T> XT(X);
			thrust::transform(XT, XT+(r*n), XT, theta<T>(x));
		}

		void roundY(const T& y) {
			thrust::device_ptr<T> YT(Y);
			thrust::transform(YT, YT+(r*m), YT, theta<T>(y));
		}
		
		struct absfunc{
			__host__ __device__
			T operator()(const T& x) const {
				return abs(x);
			}
		};

		virtual void printMatrices(){
			std::cout << "\nX: " << std::endl;
			Matrix::printGPU(X, r, n);
			std::cout << "\nY: " << std::endl;
			Matrix::printGPU(Y, m, r);
		}
		
};
#endif
